//
//  ViewController.swift
//  AppRegioes
//
//  Created by iossenac on 08/11/18.
//  Copyright © 2018 iossenac. All rights reserved.
//

import UIKit

class ViewController: UITableViewController {

    var regioes : [String] = ["Centro-Oeste", "Nordeste", "Norte",  "Sudeste", "Sul" ]
    var estados : [String] = ["Distrito Federal, Goias...",
                              "Alagoas, Bahia...",
                              "Acre, Amazonas...",
                              "Espirito Santo, Minas gerais",
                              "Paraná, Rio Grande do Sul"]
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return regioes.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celula = "celula"
        let item = tableView.dequeueReusableCell(withIdentifier: celula, for: indexPath)
        item.textLabel?.text = regioes[indexPath.row]
        return item
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     
        var celula =  sender as? UITableViewCell
        //regiao = celula?.textLabel?.text
        var regiao = celula?.textLabel?.text
        (segue.destination as? DetalhesViewController)?.regiao = regiao
        print ( regiao)
        //txtDetalhe.text  = regiao
        // segue.
    }
    
    override func performSegue(withIdentifier identifier: String, sender: Any?) {
        //
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        return
        tableView.deselectRow(at: indexPath, animated: true)
        let alerta = UIAlertController(title: "Estados", message: estados[indexPath.row], preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK",style: .default, handler: nil)
        alerta.addAction(ok)
        present(alerta, animated: true, completion: nil)
        
    }
    
    class Question {
        public var title: String?
    }
    class ApiResult {
        public var items : [Question]?
    }
    
    func loadQuestions(withCompletion completion: @escaping ([Question]?) -> Void) {
        let session = URLSession(configuration: .ephemeral, delegate: nil, delegateQueue: OperationQueue.main)
        let url = URL(string: "https://api.stackexchange.com/2.2/questions?order=desc&sort=votes&site=stackoverflow")!
        let task = session.dataTask(with: url, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) -> Void in
            guard let data = data else {
                completion(nil)
                return
            }
            guard let json = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:Any] else {
                completion(nil)
                return
            }
            
            
            let questions = (json!["items"] as! [ [String:Any] ]).map ({
                (value : [String:Any]) -> Question in
                let q = Question()
                q.title = value["title"] as! String
                return q
            })
            completion(questions)
        })
        task.resume()
    }
    
    func getResult (_ result : [Question]?) -> Void{
        print (result)
        regioes = result!.map({
            (value : Question) -> String in
            return value.title!
            
        })
        self.tableView.reloadData()
       // self.refresher.endRefreshing()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadQuestions(withCompletion:getResult)
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

